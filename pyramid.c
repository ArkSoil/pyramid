#include <stdlib.h>
#include <SDL2/SDL.h>

typedef struct geometry
{
	int a, b;
	int x, y, z;
	char type;
}geometry;

typedef struct sprite
{
	int height, width;
	char * color_pal;
	char * src;
}sprite;

typedef struct entity
{
	sprite s;
	int x,y,z;	
}entity;

typedef struct animation
{
	entity ** frames;
	int count, loop;
	float speed;
}animation;

typedef struct window
{
	char * name;
	int color;
	char decoration; 
	int height, width, x, y, z;
	struct window * windows;
	entity * entities;
	animation * animations;
	geometry * geometries;
}window;




int main()
{
	
	return 0;
}
