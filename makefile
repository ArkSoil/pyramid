OBJS = pyramid.c
OBJ_NAME = pyramid

CC = gcc

COMPILER_FLAGS = -Wall -Wextra

LINKER_FLAGS = `sdl2-config --libs`

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)

